# RECUR: R application for the bidirectional analysis and visualization of allelic imbalance profiles from multi-sample genomic data

## Authors
Yasminka A. Jakubek, F. Anthony San Lucas, Paul Scheet

## License
MIT license

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

## Description
REpeat Chromosomal changes Uncovered by Reflection (RECUR) is an R application 
for the comparative analysis of AI profiles derived from SNP and next-generation 
sequencing data. The algorithm accepts genotype calls and “B” or reference allele 
frequencies from at least 2 samples derived from the same individual. For a predefined 
set of genomic regions with AI, RECUR compares BAF shifts among samples. Each shift 
has two possible directions —an increase of the maternal, relative to the paternal, 
haplotype or vice versa — and each pair of samples can exhibit the same, or opposite, 
shift.  The latter phenomenon of bidirectional haplotype shifts has been referred to as 
“mirrored subclonal allelic imbalance” and linked to clinico-pathological features of cancer. 
The algorithm detects genomic segments where AI shifts have opposite directions between 
samples. It also plots BAF values for all samples and uses a two-color scheme for intuitive 
visualization of these bidirectional shifts.
 
## Availability and Implementation:
RECUR is available as an R application. Source code and documentation are available at scheet.org.

## Installation notes
Download the application from gitlab.  The recur.R script is written in R and is run through 
the Rscript recur.R command.  The script currently has the following package dependencies.

install.packages("argparse")
install.packages("data.table")
install.packages("plyr")

## Quick start
A simple example
Some examples are packaged together with the software.  In the following example, 2 regions of a tumor were genotyped, and recur.R is used to identify regions of the genome between the two samples that exhibit mirrored AI.
Go into the examples directory and run the test.baf.ucs_0609.sh script, which specifies the Rscript recur.R command with parameters.  A report of mirrored events and 2 figures (1 for each test sample; see above) are generated and stored in the examples/test_output directory.

./test.baf.field.simple.sh

(1/7) Loading data for normal sample: germline.gt
(2/7) Loading data for test samples
      loading test sample (1/4): tumor_1.baf
      loading test sample (2/4): tumor_2.baf
      loading test sample (3/4): tumor_3.baf
      loading test sample (4/4): tumor_4.baf
(3/7) Loading in candidate genomic regions for AI mirror assessment events.txt
      number of candidate event regions: 37
(4/7) Testing for mirroring in candidate event regions
      number of sample pairs for assessing mirroring: 6
      pairwise comparison (1/6): tumor_1.baf, tumor_2.baf
      pairwise comparison (2/6): tumor_1.baf, tumor_3.baf
      pairwise comparison (3/6): tumor_1.baf, tumor_4.baf
      pairwise comparison (4/6): tumor_2.baf, tumor_3.baf
      pairwise comparison (5/6): tumor_2.baf, tumor_4.baf
      pairwise comparison (6/6): tumor_3.baf, tumor_4.baf
(5/7) Summarizing AI mirror events
      number of putative mirrored regions = 7
      number of putative pairwise mirrored events = 18
      identifying reference sample for AI mirror region (1/7): 5:5397948-7802280
      identifying reference sample for AI mirror region (2/7): 5:32146128-35945288
      identifying reference sample for AI mirror region (3/7): 18:81092-13826521
      identifying reference sample for AI mirror region (4/7): 18:14774149-46349590
      identifying reference sample for AI mirror region (5/7): 18:46527222-77907003
      identifying reference sample for AI mirror region (6/7): 21:14685576-48051100
      identifying reference sample for AI mirror region (7/7): 6:474461-170756596
(6/7) Reporting mirror events
(7/7) Generating mirror plots
      plotting mirrored AI for (1/4): tumor_1.baf
      plotting mirrored AI for (2/4): tumor_2.baf
      plotting mirrored AI for (3/4): tumor_3.baf
      plotting mirrored AI for (4/4): tumor_4.baf

This is the command that was run by the example script:

Rscript ../recur.R/recur.R \
  --normal_sample "test_data/germline.gt" \
  --test_samples "test_data/tumor_1.baf" \
                 "test_data/tumor_2.baf" \
                 "test_data/tumor_3.baf" \
                 "test_data/tumor_4.baf" \
  --regions "test_data/events.txt" \
  --chromosomes "../resources/hg19.chromosome_lengths.txt" \
  --out_dir "test_output" \
  --out_prefix "sub_clone_baf_test_simple"

## Input files description
At a minimum, input for an analysis requires 3 types of files.  A normal sample file that provides germline heterozygous sites, B allele frequency files from 2 or more samples, and a regions file that specifies genomic regions over which mirrored AI will be tested for.

### Normal sample file
The normal sample file is used to identify germline het sites for the subject being analyzed.  Although not optimal, a tumor sample can be used in lieu of a normal sample.  A GT column is required, and the application keeps track of the coordinates of het sites.  Valid values for het sites include: A|B, B|A, A/B, B/A, 0|1, 1|0, 0/1, 1/0.  These values were selected to support standard genotype representations commonly found in genotype or variant call format files.  Note: To make het-identification more generic, we are working on a split-and-compare approach to detect allele differences.

chr     pos     GT
1       876557  B|A
1       881755  B|A
1       887651  A|B
1       916610  A|B
...

### B allele frequency files
B allele frequencies for each test sample should be specified in a separate
tab-delimited file.  These tab-delimited files require 3 columns: chr, pos and BAF.
Note: in the future, we won't require the GT column as this could be specified from a "normal" (or "germline") sample.

chr     pos     BAF
1       876557  0.4272
1       881755  0.3508
1       887651  0.6367
1       916610  0.6208
...

### Genomic regions file
A genomic region file is also required to specify regions of the genome for which mirrored AI will be tested.  This file simply requires 3 columns: chr, begin and end.
Note: in the future if this is not provided, we will test for mirrored AI at chromosome arms.

chr     begin   end
1       2381568 32109849
1       32501215        41969327
1       44440146        211898313
...


## Available parameters
Pass the --help or -h parameter to the script to see the available parameters.

Rscript recur.R -h

usage: recur.R [-h] -n NORMAL_SAMPLE -t TEST_SAMPLES [TEST_SAMPLES ...]
                     -r REGIONS [--out_dir OUT_DIR] [--out_prefix OUT_PREFIX]
                     [--ai_event_binom_pval_thresh AI_EVENT_BINOM_PVAL_THRESH]
                     [--mirror_ai_corr_pval_thresh MIRROR_AI_CORR_PVAL_THRESH]
                     [--column_baf COLUMN_BAF] [--column_lrr COLUMN_LRR]
                     [--column_chr COLUMN_CHR] [--column_pos COLUMN_POS]
                     [--column_begin COLUMN_BEGIN] [--column_end COLUMN_END]
                     [--column_switch COLUMN_SWITCH] [--haploh]

optional arguments:
  -h, --help            show this help message and exit
  -n NORMAL_SAMPLE, --normal_sample NORMAL_SAMPLE
                        File of genotype data for normal sample. (default:
                        None)
  -t TEST_SAMPLES [TEST_SAMPLES ...], --test_samples TEST_SAMPLES [TEST_SAMPLES ...]
                        Files of genotype data for samples where AI mirroring
                        will be assessed. (default: None)
  -r REGIONS, --regions REGIONS
                        Genomic regions for which AI mirroring will be
                        assessed. (default: None)
  --out_dir OUT_DIR     Output directory. (default: .)
  --out_prefix OUT_PREFIX
                        Output prefix. (default: )
  --ai_event_binom_pval_thresh AI_EVENT_BINOM_PVAL_THRESH
                        Event detection binomial p-value threshold. (default:
                        0.0001)
  --mirror_ai_corr_pval_thresh MIRROR_AI_CORR_PVAL_THRESH
                        Frequency-based haplotype correlation p-value at which
                        to call a mirroring event. (default: 0.0001)
  --column_baf COLUMN_BAF
                        Column name for B-allele frequencies. (default: BAF)
  --column_lrr COLUMN_LRR
                        Column name for log R ratios (field is optional).
                        (default: LRR)
  --column_chr COLUMN_CHR
                        Column name from chromosomes. (default: chr)
  --column_pos COLUMN_POS
                        Column name for the chromosomal position of a marker.
                        (default: pos)
  --column_begin COLUMN_BEGIN
                        Column name for the beginning of an event. (default:
                        begin)
  --column_end COLUMN_END
                        Column name for the end of an event. (default: end)
  --column_switch COLUMN_SWITCH
                        Column name for the haploh phase consistency indicator
                        (field is optional). (default: SWITCH)
  --haploh              Flag to indicate that AI event detection (in addition
                        to mirror AI assessment) should be performed. In order
                        to perform this test, the data must contain switches.
                        (default: False)



