#!/bin/bash

/usr/local/bin/Rscript ../recur/R/recur.R \
  --normal_sample "test_data/germline.gsloh" \
  --test_samples "test_data/tumor_1.gsloh" \
                 "test_data/tumor_2.gsloh" \
                 "test_data/tumor_3.gsloh" \
		 "test_data/tumor_4.gsloh" \
  --regions "test_data/events.txt" \
  --chromosomes "../resources/hg19.chromosome_lengths.txt" \
  --out_dir "test_output" \
  --out_prefix "sub_clone_baf_test" 
