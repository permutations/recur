#!/bin/bash

/usr/local/bin/Rscript ../recur/R/recur.R \
  --normal_sample "test_data/germline.gt" \
  --test_samples "test_data/tumor_1.baf" \
                 "test_data/tumor_2.baf" \
                 "test_data/tumor_3.baf" \
		 "test_data/tumor_4.baf" \
  --regions "test_data/events.txt" \
  --chromosomes "../resources/hg19.chromosome_lengths.txt" \
  --out_dir "test_output" \
  --out_prefix "sub_clone_baf_test_simple" 
